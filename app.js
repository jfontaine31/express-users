const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const cookieSession = require('cookie-session')
const lessMiddleware = require('less-middleware');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const friendsRouter = require('./routes/friends');
const authRouter = require('./routes/auth');
const app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views')); // sets the where to locate the views
app.set('view engine', 'pug'); // sets the templating engine

app.use(logger('dev'));//  no idea
app.use(express.json()); /// no idea
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser()); // no idea
app.use(lessMiddleware(path.join(__dirname, 'public'))); // parses less ????
app.use(express.static(path.join(__dirname, 'public'))); //sets the public directory
app.use(cookieSession({
    name: "session",
    keys: ["key1", "key2"],
    maxAge: 24 * 60 * 60 * 1000 // 24 hours
}));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/friends', friendsRouter);
app.use('/auth', authRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
