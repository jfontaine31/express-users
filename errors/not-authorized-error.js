
class NotAuthorizedError extends Error {
    constructor(req, ...params) {
        super(...params);
        this.date = new Date();
        this.ip = req.session;
        this.statusCode = 401;
        this.message = `user: ${this.ip} is not authorized; invalid apiKey was set`;
    }
    toJson() {
        const { date, ip, message, statusCode } = this;
        return { statusCode, date, ip, message  };
    }
}

module.exports = {
    NotAuthorizedError
};