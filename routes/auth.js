const { Router } = require('express');
const { NotAuthorizedError } = require("../errors/not-authorized-error");

const authRouter = Router();
const apiKey = "abcd";

authRouter.use(function(req, res, next) {
    // has proper api Key
    const requestApiKey = req.get('apiKey'); // set in the header
    if(requestApiKey && requestApiKey === apiKey){
        console.info('user is authorized');
        return next();
    }
    throw new NotAuthorizedError(req);
});

// error middleware
authRouter.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(err.statusCode).json(err.toJson());
});

authRouter.get('/', function(req, res) {
    res.status(400).send('You sent the proper API Key')
});



module.exports = authRouter;

