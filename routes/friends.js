const express = require('express');
const friendsRouter = express.Router();

let friends = ["Matthew", "Mark", "Luke", "John"];

friendsRouter.get('/', async function(req, res, next) {
    res.json({ friends });
});

friendsRouter.put('/add-friend/:friend', function(req, res, next) {
    const { friend } = req.params;
    friends.push(friend);
    res.status(200).send('friend added');
});

friendsRouter.delete('/delete-friend/:friend', function(req, res, next) {
    const { friend } = req.params;
    friends = friends.filter((myFriend) => myFriend !== friend);
    res.status(200).send('friend removed');
});

friendsRouter.post('/update-friend/:friend/:updatedFriend', function(req, res, next) {
    const { friend, updatedFriend } = req.params;
    const friendIndex = friends.findIndex(myFriend => myFriend === friend);

    friends[friendIndex] = updatedFriend;

    res.status(200).send('friend updated');
});

module.exports = friendsRouter;